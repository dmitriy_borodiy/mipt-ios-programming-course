//
//  Pet.h
//  FirstLesson
//
//  Created by artemiy on 20.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Animal.h"

@class Pet;

@protocol FoodProvider <NSObject>
- (void)feedMe:(Pet *)hungryPet;
@end

@interface Pet : Animal

@property (nonatomic, weak) id <FoodProvider> master;

@end
