//
//  Pet.m
//  FirstLesson
//
//  Created by artemiy on 20.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Pet.h"

@implementation Pet

- (void)setHungerLevel:(int)hungerLevel{
    [super setHungerLevel:hungerLevel];
    if (hungerLevel < 0) {
        [self.master feedMe:self];
    }
}

@end
