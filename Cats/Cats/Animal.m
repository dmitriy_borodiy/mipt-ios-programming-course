//
//  Animal.m
//  FirstLesson
//
//  Created by artemiy on 20.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Animal.h"
NSString * const AnimalHungerLevelKey = @"hunger level";
NSString * const AnimalIsMaleKey = @"animal is mail";


@implementation Animal


#pragma mark - initialisation
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (!(self = [super init])) return nil;
    _isMale      = [aDecoder decodeBoolForKey:AnimalIsMaleKey];
    _hungerLevel = [aDecoder decodeIntForKey:AnimalHungerLevelKey];
    return self;
}

#pragma mark - saving
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeBool:self.isMale forKey:AnimalIsMaleKey];
    [aCoder encodeInt:self.hungerLevel forKey:AnimalHungerLevelKey];
}

@end
