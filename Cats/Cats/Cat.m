//
//  Cat.m
//  FirstLesson
//
//  Created by artemiy on 13.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Cat.h"
NSString * const CatNickNameKey = @"cat nick name";
NSString * const CatNumberOfColorsKey = @"cat number of colors";

@implementation Cat

#pragma mark - Initialization
- (instancetype)init {
    if (!(self = [super init])) return nil;
    _numberOfColors = [Cat defaultNumberOfColors];
    _nickName = [Cat defaultCatName];
    return self;
}

- (instancetype)initWithNumberOfColors:(int)numberOfColors{
    if (numberOfColors > 3 || numberOfColors < 1) return nil;
    if (!(self = [self init])) return nil;
    _numberOfColors = numberOfColors;
    _nickName = [Cat defaultCatName];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (!(self = [super initWithCoder:aDecoder])) return nil;
    _nickName = [aDecoder decodeObjectForKey:CatNickNameKey];
    _numberOfColors = [aDecoder decodeIntForKey:CatNumberOfColorsKey];
    return self;
}

#pragma mark - saving
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.nickName forKey:CatNickNameKey];
    [aCoder encodeInteger:self.numberOfColors forKey:CatNumberOfColorsKey];
}

#pragma mark - model

- (void)setIsMale:(BOOL)isMale {
    if (_numberOfColors == 3 && isMale) {
        return;
    }
    self.isMale = isMale;
}

- (void)setNumberOfColors:(int)numberOfColors{
    if (numberOfColors > 3 || numberOfColors < 1) {
        return;
    }
    else if (numberOfColors == 3 && self.isMale){
        return;
    }
    _numberOfColors = numberOfColors;
}

- (BOOL)isEqualToCat:(Cat *)cat{
    if (self == cat) return YES;
    if (self.isMale != cat.isMale) return NO;
    if (![self.nickName isEqualToString:cat.nickName]) return NO;
    if (self.numberOfColors != cat.numberOfColors) return NO;
    
    return YES;
}

#pragma mark - other methods

+ (NSString *)defaultCatName{
    return @"murmur";
}

+ (int)defaultNumberOfColors{
    return 1;
}

- (NSString *)description{
    assert(self.nickName);
    return [NSString stringWithFormat:@"cat name:%@ %d %d", self.nickName, self.isMale, self.numberOfColors];
}

@end
