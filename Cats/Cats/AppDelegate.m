//
//  AppDelegate.m
//  Cats
//
//  Created by Artemiy on 29.09.14.
//  Copyright (c) 2014 Artemiy. All rights reserved.
//

#import "AppDelegate.h"
#import "Cat.h"

@interface AppDelegate ()
@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [self allocatingExampleCats];
    [self testStoringCats];
}

- (void)testStoringCats{
    Cat *cat = [[Cat alloc] initWithNumberOfColors:3];
    cat.isMale = YES;
    
    NSData *catInData = [NSKeyedArchiver archivedDataWithRootObject:cat];
    Cat *extractedCat = [NSKeyedUnarchiver unarchiveObjectWithData:catInData];
    assert([cat isEqualToCat:extractedCat]);
    assert(cat.isMale == NO);
}

- (void)allocatingExampleCats{
    Cat *another_cat = [[Cat alloc] init];
    assert(another_cat.numberOfColors);
    NSLog(@"%@", another_cat);

    Cat *my_cat = [[Cat alloc] init];
    my_cat.nickName = @"murzik";
    NSLog(@"%@", my_cat);
}


@end
