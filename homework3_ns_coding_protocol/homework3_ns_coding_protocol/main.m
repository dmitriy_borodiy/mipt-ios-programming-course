//
//  main.m
//  homework3_ns_coding_protocol
//
//  Created by Dmitry Borody on 10/11/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
