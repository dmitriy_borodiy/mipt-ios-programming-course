//
//  AppDelegate.m
//  homework3_ns_coding_protocol
//
//  Created by Dmitry Borody on 9/27/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "AppDelegate.h"
#import "Person.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSString * const filepath = @"/tmp/__person_test.txt";
    
    // Create the first Person object, save it to file
    Person *personOne = [[Person alloc] initWithFirstName:@"John" andSecondName:@"Smith"];
    [personOne saveToFile:filepath];
    
    // Create the second Person, load it from the file
    Person *personTwo = [Person loadFromFile:filepath];
    
    NSLog(@"%@\n%@", personOne, personTwo);
}

@end