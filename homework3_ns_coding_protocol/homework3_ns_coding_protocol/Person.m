//
// Created by Dmitry Borody on 9/27/14.
// Copyright (c) 2014 mipt. All rights reserved.
//

#import "Person.h"


@implementation Person

NSString * const kFirstName = @"firstName";
NSString * const kSecondName = @"secondName";

- (instancetype)initWithFirstName: (NSString*) firstName andSecondName: (NSString*) secondName {
    if (!(self = [super init])) return nil;
    _firstName = firstName;
    _secondName = secondName;
    return self;
}

- (void)saveToFile:(NSString *)filename {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    [data writeToFile:filename atomically:YES];
}

- (NSString*) description {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.secondName];
}

- (id) initWithCoder:(NSCoder *)decoder {
    return [self initWithFirstName:[decoder decodeObjectForKey:kFirstName]
                     andSecondName:[decoder decodeObjectForKey:kSecondName]];
}

- (void) encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:_firstName forKey:kFirstName];
    [coder encodeObject:_secondName forKey:kSecondName];
}

+ (id)loadFromFile: (NSString *) filename {
    NSData *data = [NSData dataWithContentsOfFile:filename];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

@end