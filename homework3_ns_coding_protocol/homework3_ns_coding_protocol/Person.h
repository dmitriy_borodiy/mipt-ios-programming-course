//
// Created by Dmitry Borody on 9/27/14.
// Copyright (c) 2014 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Person : NSObject <NSCoding>

@property NSString *firstName;
@property NSString *secondName;

- (instancetype)initWithFirstName: (NSString*) firstName andSecondName: (NSString*) secondName;
- (id)initWithFilename:(NSString *const)filepath;

- (void) saveToFile: (NSString *) filename;

+ (id)loadFromFile: (NSString *) filename;

@end