//
//  AppDelegate.h
//  homework3_ns_coding_protocol
//
//  Created by Dmitry Borody on 10/11/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
