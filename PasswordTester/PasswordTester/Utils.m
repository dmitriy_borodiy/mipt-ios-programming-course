//
//  Utils.m
//  PasswordTester
//
//  Created by Dmitry Borody on 10/23/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Utils.h"

@interface NSString (ConvertToArray)
-(NSArray *)convertToArray;
@end

@implementation NSString (ConvertToArray)

-(NSArray *)convertToArray
{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (int i=0; i < self.length; i++) {
        NSString *tmp_str = [self substringWithRange:NSMakeRange(i, 1)];
        [arr addObject:[tmp_str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    return arr;
}
@end

@implementation Utils

+ (float) computePasswordEntropy: (NSString*) password
{
    int numberOfUsedCharacters = [[[NSSet alloc] initWithArray:[password convertToArray]] count];
    return password.length * log2(numberOfUsedCharacters);
}

@end;