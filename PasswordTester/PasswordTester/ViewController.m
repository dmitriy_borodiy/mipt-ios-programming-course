//
//  ViewController.m
//  PasswordTester
//
//  Created by Dmitry Borody on 10/23/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "ViewController.h"
#import "Utils.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIProgressView *passwordProgress;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *strengthLabel;

@end



@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self changeStatus:@"" Animated: NO WithProgress:0.0 AndColor:[UIColor grayColor]];
    
    self.passwordTextField.delegate = self;
}

- (void)changeStatus: (NSString*) text Animated: (BOOL) animated WithProgress: (float) progress AndColor: (UIColor*) color
{
    if (!animated) {
        self.strengthLabel.text = text;
        self.strengthLabel.textColor = color;
        self.passwordProgress.progress = progress;
        self.passwordProgress.progressTintColor = color;
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            [self.passwordProgress setProgress:progress
                                      animated:YES];
        }];
        if (![text isEqualToString:self.strengthLabel.text]) {
            [UIView animateWithDuration:0.2 animations:^{
                self.strengthLabel.frame =
                    CGRectMake(0,
                               self.strengthLabel.frame.origin.y,
                               self.strengthLabel.frame.size.width,
                               self.strengthLabel.frame.size.height);
                self.strengthLabel.alpha = 0.0;
            } completion:^(BOOL finished) {
                self.strengthLabel.frame =
                    CGRectMake(200,
                               self.strengthLabel.frame.origin.y,
                               self.strengthLabel.frame.size.width,
                               self.strengthLabel.frame.size.height);
                
                self.strengthLabel.text = text;
                self.strengthLabel.textColor = color;
                self.passwordProgress.progressTintColor = color;
                
                [UIView animateWithDuration:0.2 animations:^{
                    self.strengthLabel.frame =
                    CGRectMake(100,
                               self.strengthLabel.frame.origin.y,
                               self.strengthLabel.frame.size.width,
                               self.strengthLabel.frame.size.height);
                    self.strengthLabel.alpha = 1.0;
                }];
            }];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSMutableString *newText = [[NSMutableString alloc] initWithString:textField.text];
    [newText replaceCharactersInRange:range withString:string];
    
    if (newText.length == 0) {
        [self changeStatus:@"" Animated: YES  WithProgress:0.0 AndColor:[UIColor grayColor]];
        return YES;
    }
    
    float entropy = [Utils computePasswordEntropy:newText];
    float progress = (entropy + 1.0) / 70.0;
    if (progress > 1.0)
        progress = 1.0;
    
    if (entropy < 20) {
        [self changeStatus:@"Low" Animated: YES  WithProgress:progress AndColor:[UIColor redColor]];
    } else if (entropy < 50) {
        [self changeStatus:@"Medium" Animated: YES  WithProgress:progress AndColor:[UIColor orangeColor]];
    } else {
        [self changeStatus:@"High" Animated: YES WithProgress:progress AndColor:[UIColor greenColor]];
    }
    
    return YES;
}


@end
