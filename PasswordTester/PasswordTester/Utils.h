//
//  Utils.h
//  PasswordTester
//
//  Created by Dmitry Borody on 10/23/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject
+ (float) computePasswordEntropy: (NSString*) password;
@end
