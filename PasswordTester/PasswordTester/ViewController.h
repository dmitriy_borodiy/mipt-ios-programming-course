//
//  ViewController.h
//  PasswordTester
//
//  Created by Dmitry Borody on 10/23/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@end
