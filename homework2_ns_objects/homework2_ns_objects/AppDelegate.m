//
//  AppDelegate.m
//  homework2_ns_objects
//
//  Created by Dmitry Borody on 9/27/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "AppDelegate.h"
#import "Person.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSString * const filepath = @"/tmp/__person_test.txt";

    // Create the first Person object, save it to file
    Person *p1 = [[Person alloc] initWithFirstName:@"John" andSecondName:@"Smith"];
    [p1 saveToFile:filepath];

    // Create the second Person, load it from the file
    Person *p2 = [[Person alloc] initWithFilename:filepath];

    NSLog(@"%@\n%@", p1, p2);
}

@end