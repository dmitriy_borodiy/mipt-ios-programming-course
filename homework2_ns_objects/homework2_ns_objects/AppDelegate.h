//
//  AppDelegate.h
//  homework2_ns_objects
//
//  Created by Dmitry Borody on 9/27/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end