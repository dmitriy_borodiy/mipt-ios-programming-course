//
// Created by Dmitry Borody on 9/27/14.
// Copyright (c) 2014 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Person : NSObject

@property NSString *firstName;
@property NSString *secondName;

- (instancetype)initWithFirstName: (NSString*) firstName andSecondName: (NSString*) secondName;
- (instancetype) initWithFilename: (NSString *) filename;
- (void) saveToFile: (NSString *) filename;

@end