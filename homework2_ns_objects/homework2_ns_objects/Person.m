//
// Created by Dmitry Borody on 9/27/14.
// Copyright (c) 2014 mipt. All rights reserved.
//

#import "Person.h"


@implementation Person

NSString * const kFirstName = @"firstName";
NSString * const kSecondName = @"secondName";

- (instancetype)initWithFirstName: (NSString*) firstName andSecondName: (NSString*) secondName {
    if (!(self = [super init])) return nil;
    _firstName = firstName;
    _secondName = secondName;
    return self;
}

- (instancetype)initWithFilename:(NSString *)filename {
    if (!(self = [super init])) return nil;
    NSDictionary *state = [NSDictionary dictionaryWithContentsOfFile:filename];
    _firstName = state[kFirstName];
    _secondName = state[kSecondName];
    return self;
}

- (void)saveToFile:(NSString *)filename {
    NSMutableDictionary *state = [[NSMutableDictionary alloc] init];
    if (self.firstName)
        [state setValue:self.firstName forKey:kFirstName];
    if (self.secondName)
        [state setValue:self.secondName forKey:kSecondName];
    [state writeToFile:filename atomically:YES];
}

- (NSString*) description {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.secondName];
}

@end