//
//  main.m
//  homework2_ns_objects
//
//  Created by Dmitry Borody on 9/27/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}