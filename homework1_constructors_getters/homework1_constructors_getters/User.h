//
//  User.h
//  homework1_constructors_getters
//
//  Created by Dmitry Borody on 9/20/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property(nonatomic) NSString *login;
@property(nonatomic) NSString *email;
@property(nonatomic) BOOL hasSubscription;

- (id) initWithLogin: (NSString *) login andEmail: (NSString *) email;

@end
