//
//  main.m
//  homework1_constructors_getters
//
//  Created by Dmitry Borody on 9/20/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
