//
//  AppDelegate.m
//  homework1_constructors_getters
//
//  Created by Dmitry Borody on 9/20/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "AppDelegate.h"
#import "User.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    User *user1 = [[User alloc] initWithLogin: @"mylogin" andEmail:@"myemail@example.com"];
    NSLog(@"user1: %@", user1);
                   
    [user1 setHasSubscription: NO];
    [user1 setLogin: @"my new login"];
    
    NSLog(@"user1's login: %@", user1.login);
}

@end
