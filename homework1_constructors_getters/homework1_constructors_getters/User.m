//
//  User.m
//  homework1_constructors_getters
//
//  Created by Dmitry Borody on 9/20/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "User.h"

@implementation User

- (instancetype) initWithLogin: (NSString*)login andEmail: (NSString*)email {
    if (!(self = [super init])) return nil;
    _login = login;
    _email = email;

    // Imagine that business logic dictates that for user to have a subscription,
    // he must have a login and an email
    if (_email != nil && _login != nil) {
        _hasSubscription = YES;
    }
    return self;
}

-(void) setLogin:(NSString *)login {
    NSLog(@"Setting login to %@", login);
    _login = login;
    if (!_email)
        _hasSubscription = NO;
}

-(void) setEmail:(NSString *)email {
    NSLog(@"Setting email to %@", email);
    _email = email;
    if (!_email == nil)
        _hasSubscription = NO;
}

-(void) setHasSubscription:(BOOL)hasSubscription {
    if (_email != nil && _login != nil) {
        NSLog(@"Setting hasSubscription to %d", hasSubscription);
        _hasSubscription = hasSubscription;
    }
}

- (NSString*) description {
    return [NSString stringWithFormat:@"login, email, subsciption:%@ %@ %d", self.login, self.email, self.hasSubscription];
}

@end

