//
//  AppDelegate.h
//  homework1_constructors_getters
//
//  Created by Dmitry Borody on 9/20/14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
